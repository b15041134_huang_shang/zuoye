//
//  main.cpp
//  四种循环.cpp
//
//  Created by 黄靖文 on 2018/6/6.
//  Copyright © 2018年 黄靖文. All rights reserved.
#include<iostream>
using namespace std;
int main()
{
    int x;
    int sum=0,sum1=0,sum2=0,sum3=0;
    cout<<"1.Please input an integer:"<<endl;
    cin>>x;
    while(x>0)
    {
        sum+=x%10;
        x=x/10;
        
    }
    cout<<"WHILE循环:"<<sum<<endl;
    
    cout<<"2.Please input an integer:"<<endl;
    cin>>x;
    for(;x;x/=10)
    {
        sum1+=x%10;
    }
    cout<<"FOR循环"<<sum1<<endl;
    
    cout<<"3.Please input an integer:"<<endl;
    cin>>x;
    do
    {
        sum2+=x%10;
        x/=10;
    }while(x);
    cout<<"DO WHILE循环:"<<sum2<<endl;
    
    cout<<"4.Please input an integer:"<<endl;
    cin>>x;
    while(1)
    {
        sum3+=x%10;
        x=x/10;
        if(x==0)
            break;
    }
    cout<<"WHILE（1）循环:"<<sum3<<endl;
    return 0;
}
