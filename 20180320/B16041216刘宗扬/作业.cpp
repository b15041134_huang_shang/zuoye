#include<iostream>
#include<math.h>
using namespace std;

void func1(int x){
    int m=x;
    int sum=0;
    for(;x!=0;x=x/10){
        sum+=x%10;
    }
    cout<<"sum="<<sum<<endl;
    sum=0;
    x=m;
    while(x!=0){
        sum+=x%10;
        x=x/10;
    }
    cout<<"sum="<<sum<<endl;
    sum=0;
    x=m;
    do{
        sum+=x%10;
        x=x/10;
    }while(x!=0);
    cout<<"sum="<<sum<<endl;
    sum=0;
    x=m;
    while(1){
        if(x>0){
            sum+=x%10;
            x=x/10;
        }
        else if(x==0)
            break;
    }
    cout<<"sum="<<sum<<endl;
}

void func2(){
    int count=0;
    for(int i=1;i<=1000;i++){
        int iszs=1;
        for(int j=2;j<=sqrt(i);j++){
            if(i%j==0){
               iszs=0;
            }
        }
        if(iszs==1)
        {   count++;
            if(count%5==0)
                cout<<i<<endl;
            else
                cout<<i<<" ";
        }
    }
}


int main(){
  /*  int x;
    cin>>x;
    func1(x);*/
    func2();
    return 0;
}
