#include<stdio.h>

int isPrime(int n)
{
    int i;
    int top = n/2;
    for (i = 2; i < top; i++)
    {
        if (!(n%i))
            return 0;
    }
    return 1;
}

int main(void)
{
    int i;
    int count = 1;
    for (i = 1; i < 1000; i++)
    {
        if (isPrime(i))
        {
            printf("%3d ", i);
            count++;
        }
        if (!(count%6))
        {
            printf("\n");
            count++;
        }
    }
    return 0;
}
