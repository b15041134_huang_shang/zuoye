#include<iostream>
using namespace std;
void reverse(char * src){
	char * start = src;
	char * flag = src;
	while(*flag)flag++;
	flag --;
	while(start < flag){
		char tmp = * start ;
		*start = * flag;
		*flag = tmp;
		start++;
		flag--;
	}
}
void encrypt(char * src){
	reverse(src);
	char * flag=src;
	for(;*flag;flag++){
		(*flag) = (*flag >='a'&& *flag <='z')?((*flag)-'a'+3)%26+'a'-0x20:(*flag >='A'&& *flag <='Z')?((*flag)-'A'+3)%26+'A'+0x20:0;		
	}

}
int main(){
	char s[50];
	cout<<"Input:";
	cin>>s;
	encrypt(s);
	cout<<"Output:"<<s<<endl;
	return 0;
}