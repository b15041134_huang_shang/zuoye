// homework: 0410 B16041221

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *encryption(const char *s) {
	int str_len = strlen(s);
	char *temp = malloc(sizeof(char) * (str_len + 1));
	
	for(int i = str_len-1; i >= 0; i--){
		if (s[str_len-i-1] <= 'z' && s[str_len-i-1] >= 'a')
			temp[i] = (s[str_len-i-1] - 'a' + 3 + 26) % 26 + 'A';
		else if (s[str_len-i-1] <= 'Z' && s[str_len-i-1] >= 'A')
			temp[i] = (s[str_len-i-1] - 'A' + 3 + 26) % 26 + 'a';
	}
	temp[str_len] = '\0';
	return temp;
}

int main() {
	char s1[100];
	gets(s1);
	char *result = encryption(s1);
	puts(result);
	return 0;
}
