#include<iostream>
using namespace std;
char *miwen(char *s,int i){
	int j;
	char t;
	for(j=0;j<i;j++){					//左移3位 
		if(s[j]<='z'&&s[j]>='a')
			s[j]=(s[j]-'a'+23)%26+'a';
		else if(s[j]<='Z'&&s[j]>='A')
			s[j]=(s[j]-'A'+23)%26+'A';
	}
	for(j=0;j<i/2;j++){					//逆置 
		t=s[i-j-1];
		s[i-j-1]=s[j];
		s[j]=t; 
	} 
	for(j=0;j<i;j++){                  //大小写反转 
		if(s[j]<='Z'&&s[j]>='A')
			s[j]=s[j]+32;
		else if(s[j]<='z'&&s[j]>='a')
			s[j]=s[j]-32;
	}
	return s;
}
int main(){
	int i=0;
	char s[50]="";
	cout<<"请输入一串字符串（长度小于50且只包含大小写字母）："<<endl;
	while((s[i]=getchar())!='\n'){
		i++;
	}
	miwen(s,i);
	cout<<s;
	return 0;
}
