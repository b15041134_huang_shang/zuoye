#include<iostream>
using namespace std;
double Average(int *a,int n)
{
    int min,max,count=0;
	double average,sum=0;
	min=a[0];
	max=a[0];
	for(int i=0;i<n;i++)     //判断并记录最大分数和最小分数
	{
		if(a[i]<min)
            min=a[i];
		if(a[i]>max)
            max=a[i];
	}
	for(int j=0;j<n;j++)     //将最高分和最低分赋值为零，即去掉最高/低分
	{
		if(a[j]==min)
		{
			a[j]=0;
			count++;
		}

		if(a[j]==max)
		{
			a[j]=0;
			count++;
		}
	}
	for(int k=0;k<n;k++)
	{
		sum+=a[k];
	}
	average=sum/(n-count);
	return average;
}
int main()
{
    int i,a[9];
	double average;
	cout<<"请输入9个评分："<<endl;
	for(i=0;i<9;i++)
        cin>>a[i];
	average=Average(a,9);
	for(int i=0;i<9;i++)
	{
		cout<<a[i]<<"  ";
	}
	cout<<endl;
	cout<<"最后得分为："<<average;
	return 0;
}
