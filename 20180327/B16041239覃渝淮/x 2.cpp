#include<stdio.h>
int main()
{
    char in;
    while((in=getchar())!= '\0')
    {
		if(in>='a'&&in<='z')
			in=(in-'a'+2)%26+'a';
		if(in>='A'&&in<='Z')
			in=(in-'A'+2)%26+'A';
		if(in>='0'&&in<='9')
			in=(in-'0'+2)%10+'0';
        putchar(in);
    }
}
