//
//  main.c
//  实验二1.c
//
//  Created by 黄靖文 on 2018/3/27.
//  Copyright © 2018年 黄靖文. All rights reserved.
//使用c语言实现：输入多个字符（小写字母），进行如下变换后输出：
/*
 输入：abcxyz
 输出：cdezab
 使用getchar（）putchar（）函数
 */

#include <stdio.h>

int main()
{
    char c,d;
    c=getchar();
    while (c != '\n')
    {
        d = (c+2-'a')%26+'a';
        putchar(d);
        c=getchar();
    }
    return 0;
}
