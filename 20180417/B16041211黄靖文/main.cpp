//
//  main.cpp
//  417.cpp
//
//  Created by 黄靖文 on 2018/4/23.
//  Copyright © 2018年 黄靖文. All rights reserved.
//

#include <iostream>
using namespace std;

int main() {
    char str1[100], str2[100][100];
    int i, j, sign = 0, row = 0, col = 0;
    
    cin.getline(str1, 100);
    
    cout << endl;
    cout << "您输入的句子是：" << str1 << endl;
    cout << endl;
    
    for(i = 0; i < 100; i++){
        if(str1[i] == ' ' || str1[i] == ',' || str1[i] == '.' || str1[i] == '?' || str1[i] == '!'){
            if(i > 0 && (str1[i-1] == ' ' || str1[i-1] == ',' || str1[i-1] == '.' || str1[i-1] == '?' || str1[i-1] == '!')){
                continue;
            }
            else{
                sign++;
                str2[row][col] = '\0';
                row++;
                col = 0;
                continue;
            }
        }
        else{
            str2[row][col] = str1[i];
            col++;
        }
    }
    
    cout << "分离得到的单词如下：" << endl;
    for(i = 0; i < sign; i++){
        for(j = 0; str2[i][j]; j++){
            cout << str2[i][j];
        }
        cout << endl;
    }
    
    for(i = 0; i < sign; i++){
        for(j = 0; str2[i][j] != '\0'; j++);
        int end = j - 1;
        for(int z = 0; z < j / 2; z++, end--){
            char temp = str2[i][z];
            str2[i][z] = str2[i][end];
            str2[i][end] = temp;
        }
    }
    
    cout << "将单词逆序后输出：" << endl;
    for(i = 0; i < sign; i++){
        for(j = 0; str2[i][j]; j++){
            cout << str2[i][j];
        }
        cout << " ";
    }
    cout << endl;
    
    return 0;
}
