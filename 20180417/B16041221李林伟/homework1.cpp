#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;

void reverse(char *str, int len) {  // 反转函数，将单词反转
	for (int i = 0; i < len / 2; i++) {
		cout << str[i] << "----" << str[len-i-1] << endl;
		char temp = str[i];
		str[i] = str[len - i - 1];
		str[len - i - 1] = temp;
	}
}

void operate(char *str, int len_str) {
	int begin = 0, len = 0;
	int sign = 0;  // 标志位，控制一个单词的起始
	for (int i = 0; i < len_str; i++) {
		if (str[i] == ' ' || str[i] == '!' && sign) {  // 存在标志位,意味着有字符串
			reverse(str + begin, len);  // 反转单词
			len = 0;
			sign = 0;
		}
		if(str[i] != ' ' && str[i] != '!'){  // 字母统计
			len += 1;
			if(sign == 0){  // 新单词的开头
				begin = i;
				sign = 1;
			} 
		}
	}

	printf("%s\n", str);  // 输出字符串
}

int main()
{
	char str[50];
	char temp;
	int i = 0;
	while(scanf("%c", &temp) == 1 && temp != '\n'){
		str[i++] = temp;
	}
	str[i] = '\0';
	int len_str = strlen(str);
	operate(str, len_str);
    return 0;
}


