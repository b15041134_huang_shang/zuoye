#include<iostream>
#include<iomanip>
using namespace std;
int m[4][4]={{1,2,3,4},{9,10,11,12},{13,9,5,1},{15,11,7,3}};
void swap1(int **a,int **b,int n){

	for(int i=0;i<n;i++)
	{
		for(int j=0;j<n;j++)
		{
			b[n-1-j][i]=a[i][j];
		}
	}
}
void swap2(int **a,int **b,int n)
{
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<n;j++)
		{
			b[j][n-1-i]=a[i][j];

		}
	}

}
int main()
{   
	int **a = new int * [4];
	int **b = new int * [4];
	for(int i=0;i<4;i++)
	{
		b[i]=new int [4];
	}
	for(int i=0;i<4;i++)
	{
		a[i]=m[i];
	}
	cout<<"original:"<<endl;
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			cout<<setw(4)<<a[i][j];
		}
		cout<<endl;
	}
	cout<<"Clockwise:"<<endl;
	swap2(a,b,4);
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			cout<<setw(4)<<b[i][j];
		}
		cout<<endl;
	}
	cout<<"Counterclockwise:"<<endl;
	swap1(a,b,4);
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			cout<<setw(4)<<b[i][j];
		}
		cout<<endl;
	}
	return 0;
}